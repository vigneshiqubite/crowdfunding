package com.fundistan.crowdfunding.controller;

import com.fundistan.crowdfunding.model.HospitalsDao;
import com.fundistan.crowdfunding.model.RelationsDao;
import com.fundistan.crowdfunding.repository.HospitalsRepo;
import com.fundistan.crowdfunding.repository.RelationsRepo;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class MetadataController {

    @Autowired
    private RelationsRepo relationsRepo;
    @Autowired
    private HospitalsRepo hospitalsRepo;



    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    public String getAllFundrice() {


        return "hi";
    }

    @RequestMapping(value = "/relations", method = RequestMethod.GET)
    public Iterable<RelationsDao> getAllRelations() {


        return relationsRepo.findAll();
    }
    @RequestMapping(value = "/hospitals", method = RequestMethod.GET)
    public Iterable <HospitalsDao>getAllHospitals() {


        return hospitalsRepo.findAll();
    }


}
