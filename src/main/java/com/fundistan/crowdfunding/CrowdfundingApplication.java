package com.fundistan.crowdfunding;

import org.kamranzafar.spring.wpapi.client.WpApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan("org.kamranzafar.spring.wpapi")

public class CrowdfundingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrowdfundingApplication.class, args);
    }
    @Autowired
    private WpApiClient wpApiClient;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


}
