package com.fundistan.crowdfunding.repository;

import com.fundistan.crowdfunding.model.HospitalsDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface HospitalsRepo extends CrudRepository<HospitalsDao, Integer> {

}
