package com.fundistan.crowdfunding.repository;

import com.fundistan.crowdfunding.model.RelationsDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelationsRepo extends CrudRepository<RelationsDao, Integer> {
}
